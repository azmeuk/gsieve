#
# This Python library is a part of
#   gsieve: a Sieve script editor
#
# Copyright 2001,2002 Karsten Petersen
#
# This library is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Author(s): Karsten Petersen <kapet@kapet.de>
#

import gtk, gnome.ui
import GladeWidget

from _gsieve import *


class LoginView(GladeWidget.GladeWidget):
    def __init__(self, model):
        self.initialize("LoginView")
        self.model = model

        server, port, username, remember, password = self.model.get_basic_config()

        w = self.get_widget("input_server")
        w.set_text(server)
        w.set_width_chars(max(len(server) + 1, 30))

        self.get_widget("input_port").set_value(port)

        w = self.get_widget("input_username")
        w.set_text(username)
        w.set_width_chars(max(len(username) + 1, 30))

        self.get_widget("input_rememberpass").set_active(remember)

        w = self.get_widget("input_password")
        w.set_text(password)
        w.set_width_chars(max(len(password) + 1, 30))

        self.get_widget("input_password").grab_focus()

    def on_exit(self, *args):
        self.model.shutdown()

    def on_login(self, *args):
        server = self.get_widget("input_server").get_text()
        port = self.get_widget("input_port").get_value_as_int()
        username = self.get_widget("input_username").get_text()
        remember = self.get_widget("input_rememberpass").get_active()
        password = self.get_widget("input_password").get_text()
        self.model.set_basic_config(server, port, username, remember, password)
        self.model.login(password)
