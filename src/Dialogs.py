#
# This Python library is a part of
#   gsieve: a Sieve script editor
#
# Copyright 2001,2002 Karsten Petersen
#
# This library is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Author(s): Karsten Petersen <kapet@kapet.de>
#

import gtk
import GladeDialog

import string


class NameDialog(GladeDialog.GladeDialog):
    def __init__(self, parent, oldname=None):
        self.initialize("NameDialog")
        self.widget.set_transient_for(parent)

        self.input_name_widget = self.get_widget("input_name")
        self.insert_text_id = self.input_name_widget.connect(
            "insert_text", self.on_input_name_insert_text
        )

        self.okbutton = self.get_widget("okbutton")
        self.okbutton.set_sensitive(0)
        if oldname:
            self.input_name_widget.set_text(oldname)
        self.name = None

    def on_input_name_changed(self, *args):
        if len(self.input_name_widget.get_text()):
            self.okbutton.set_sensitive(1)
        else:
            self.okbutton.set_sensitive(0)

    def on_input_name_insert_text(self, widget, text, length, *args):
        text = text[:length]
        pos = widget.get_position()
        widget.emit_stop_by_name("insert_text")
        gtk.idle_add(self._insert, widget, text, pos)

    def _insert(self, widget, text, pos):
        ntext = ""
        for c in text:
            if ("a" <= c <= "z") or ("A" <= c <= "Z"):
                ntext += c
        widget.handler_block(self.insert_text_id)
        widget.insert_text(ntext, pos)
        widget.handler_unblock(self.insert_text_id)
        widget.set_position(pos + len(ntext))

    def on_ok(self, *args):
        self.name = self.input_name_widget.get_text()


class ErrorDialog(GladeDialog.GladeDialog):
    def __init__(self, parent, text):
        self.widget = gtk.MessageDialog(
            type=gtk.MESSAGE_ERROR, buttons=gtk.BUTTONS_OK, message_format=text
        )
        self.widget.set_transient_for(parent)


class QuestionDialog(GladeDialog.GladeDialog):
    def __init__(self, parent, text):
        self.widget = gtk.MessageDialog(
            type=gtk.MESSAGE_QUESTION,
            buttons=gtk.BUTTONS_OK_CANCEL,
            message_format=text,
        )
        self.widget.set_transient_for(parent)
        self.widget.label.set_use_markup(gtk.TRUE)
