#
# This Python library is a part of
#   gsieve: a Sieve script editor
#
# Copyright 2001,2002 Karsten Petersen
#
# This library is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Author(s): Karsten Petersen <kapet@kapet.de>
#

import gtk, gobject, gnome.ui
import GladeWidget

from _gsieve import *

COLUMN_ACTIVE = 0
COLUMN_NAME = 1


class ServerView(GladeWidget.GladeWidget):
    def __init__(self, model):
        self.initialize("ServerView")
        self.model = model

        # get the treeview, initialize the list model
        self.treeview = self.get_widget("treeview")
        self.scriptlist = gtk.ListStore(gobject.TYPE_BOOLEAN, gobject.TYPE_STRING)
        self.treeview.set_model(self.scriptlist)

        # create the list columns
        renderer = gtk.CellRendererToggle()
        renderer.connect("toggled", self.active_toggled, self.scriptlist)
        renderer.set_radio(True)
        column = gtk.TreeViewColumn(_("Active"), renderer, active=COLUMN_ACTIVE)
        column.set_clickable(True)
        self.treeview.append_column(column)

        renderer = gtk.CellRendererText()
        column = gtk.TreeViewColumn(_("Name"), renderer, text=COLUMN_NAME)
        self.treeview.append_column(column)

        # connect selection handling signals
        self.selection = self.treeview.get_selection()
        self.selection.set_mode(gtk.SELECTION_SINGLE)
        self.selection.connect("changed", self.selection_changed)
        self.get_widget("openbutton").set_sensitive(0)
        self.get_widget("deletebutton").set_sensitive(0)

        self.refresh()

    def refresh(self):
        scripts = self.model.list()
        self.scriptlist.clear()
        for s in scripts:
            name, active = s
            iter = self.scriptlist.append()
            self.scriptlist.set(iter, COLUMN_ACTIVE, active, COLUMN_NAME, name)
        self.treeview.queue_draw()

    def selection_changed(self, *args):
        model, iter = self.selection.get_selected()
        if iter:
            s = 1
        else:
            s = 0
        self.get_widget("openbutton").set_sensitive(s)
        self.get_widget("deletebutton").set_sensitive(s)

    def active_toggled(self, cell, path, model):
        iter = model.get_iter((int(path),))
        active = model.get_value(iter, COLUMN_ACTIVE)
        if not active:
            name = model.get_value(iter, COLUMN_NAME)
            self.model.activate(name)
            self.refresh()

    def on_close(self, *args):
        self.model.logout()

    def on_new(self, *args):
        self.model.new()

    def on_open(self, *args):
        model, iter = self.selection.get_selected()
        if iter:
            name = self.scriptlist.get_value(iter, COLUMN_NAME)
            self.model.edit(name)
        else:
            # uh - this should not happen!
            pass

    def on_delete(self, *args):
        model, iter = self.selection.get_selected()
        if iter:
            name = self.scriptlist.get_value(iter, COLUMN_NAME)
            self.model.delete(name)
            self.refresh()
        else:
            # uh - this should not happen!
            pass
