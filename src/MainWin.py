#
# This Python library is a part of
#   gsieve: a Sieve script editor
#
# Copyright 2001,2002 Karsten Petersen
#
# This library is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Author(s): Karsten Petersen <kapet@kapet.de>
#

import sys
import gtk, gnome.ui, gobject
import GladeWindow

# import LoginView

from _gsieve import *


class MainWin(GladeWindow.GladeWindow):
    def __init__(self, model):
        self.initialize("MainWindow")
        self.model = model

        self.model.register_mainwin(self.widget)

        self.appbar = self.get_widget("appbar")
        self.model.register_status(self.appbar.set_status)

        self.notebook = self.get_widget("notebook")
        self.notebook.remove_page(0)
        self.model.register_viewlist(self.addview, self.delview)

        self.model.startup()

    def addview(self, widget, label):
        self.notebook.append_page(widget, gtk.Label(label))

    def delview(self, widget=None, label=None):
        if widget:
            p = self.notebook.page_num(widget)
            if p != -1:
                self.notebook.remove_page(p)

        elif label:
            p = 0
            while 1:
                c = self.notebook.get_nth_page(p)
                if not c:
                    break
                if self.notebook.get_tab_label(c).get_text() == label:
                    self.notebook.remove_page(p)
                    break
                p += 1

    def on_notebook_switch_page(self, *args):
        self.appbar.set_status(u"")

    def on_about(self, *args):
        dlg = gnome.ui.About(
            PROGRAM_NAME,
            PROGRAM_VERSION,
            "(C) 2002 Karsten Petersen",
            _("gsieve is an easy-to-use editor for Sieve-scripts."),
            ["Karsten Petersen <kapet@kapet.de>"],
        )
        #                ["doc1", "doc2"],
        #                "trans")
        dlg.run()
        dlg.destroy()

    def on_quit(self, *args):
        self.model.shutdown()
