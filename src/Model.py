#
# This Python library is a part of
#   gsieve: a Sieve script editor
#
# Copyright 2001,2002 Karsten Petersen
#
# This library is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Author(s): Karsten Petersen <kapet@kapet.de>
#

import gconf, gtk

import socket, sieve
import binascii

import Dialogs
import LoginView
import ServerView
import ScriptView

from _gsieve import *


class Model:
    def __init__(self):
        self._init_config()
        self._init_status()
        self._init_viewlist()
        self._init_logic()

    #########################################################################
    ### Configuration

    def _init_config(self):
        self.config = gconf.client_get_default()
        self.config.add_dir(CONFIG_BASE, gconf.CLIENT_PRELOAD_NONE)
        self._read_basic_config()

    def _read_basic_config(self):
        server = self.config.get_string(CONFIG_SERVER)
        port = self.config.get_int(CONFIG_PORT)
        username = self.config.get_string(CONFIG_USERNAME)
        remember = self.config.get_bool(CONFIG_REMEMBERPASS)
        if remember:
            password = self.config.get_string(CONFIG_PASSWORD)
        else:
            password = u""
        try:
            password = binascii.a2b_hex(password)
        except:
            password = u""
        self.basic_config = (server, port, username, remember, password)

        def blank(s):
            if s is None:
                return ""
            else:
                return s

        self.basic_config = tuple([blank(s) for s in self.basic_config])

    def get_basic_config(self):
        return self.basic_config

    def set_basic_config(self, server, port, username, remember, password):
        self.config.set_string(CONFIG_SERVER, server)
        self.config.set_int(CONFIG_PORT, port)
        self.config.set_string(CONFIG_USERNAME, username)
        self.config.set_bool(CONFIG_REMEMBERPASS, remember)
        if not remember:
            password = u""
        self.basic_config = (server, port, username, remember, password)
        password = binascii.b2a_hex(password)
        self.config.set_string(CONFIG_PASSWORD, password)

    #########################################################################
    ### Status

    def _init_status(self):
        self.statuslist = []

    def register_status(self, setfunc):
        self.statuslist.append(setfunc)

    def set_status(self, text):
        for s in self.statuslist:
            s(text)

    #########################################################################
    ### viewlist's

    def _init_viewlist(self):
        self.view_add = self.view_del = None
        self.master_view = None
        self.slave_view = []

    def register_viewlist(self, addfunc, delfunc):
        self.view_add = addfunc
        self.view_del = delfunc

    def set_master(self, win, title):
        if self.master_view:
            self.view_del(self.master_view.widget)
        self.view_add(win.widget, title)
        self.master_view = win

    def add_slave(self, win, title):
        self.slave_view.append(win)
        self.view_add(win.widget, title)

    def del_slave(self, win):
        self.slave_view.remove(win)
        self.view_del(win.widget)

    def rename_slave(self, win, newtitle):
        self.view_del(win.widget)
        self.view_add(win.widget, newtitle)

    #########################################################################
    ### Helper Functions

    def register_mainwin(self, mainwin):
        self.mainwin = mainwin

    def error(self, text):
        dlg = Dialogs.ErrorDialog(self.mainwin, text)
        dlg.run()
        dlg.destroy()

    def question(self, text):
        dlg = Dialogs.QuestionDialog(self.mainwin, text)
        response = dlg.run()
        dlg.destroy()
        if response == gtk.RESPONSE_OK:
            return 0
        else:
            return 1

    #########################################################################
    ### Sieve connection layer

    def _sieve_errdlg(self, status):
        self.error(unicode(status[2]))

    def sieve_login(self, password):
        server, port, username, _r, _p = self.get_basic_config()

        self.sieve = sieve.Sieve(server, port)

        try:
            (status, caps) = self.sieve.login()
        except socket.error as msg:
            status = ("NO", "", "%s : %u\n%s" % (server, port, msg[1]))
        except socket.herror as msg:
            status = ("NO", "", "%s : %u\n%s" % (server, port, msg[1]))
        except socket.gaierror as msg:
            status = ("NO", "", "%s : %u\n%s" % (server, port, msg[1]))

        if status[0] != "OK":
            self._sieve_errdlg(status)
            self.sieve.logout()
            return 1

        (status,) = self.sieve.authenticate(username, password)
        if status[0] != "OK":
            self._sieve_errdlg(status)
            self.sieve.logout()
            return 1

        return 0

    def sieve_list(self):
        (status, scripts) = self.sieve.listscripts()
        if status[0] != "OK":
            self._sieve_errdlg(status)
            return []
        return scripts

    def sieve_activate(self, name):
        (status,) = self.sieve.setactive(name)
        if status[0] != "OK":
            self._sieve_errdlg(status)
            return 1
        return 0

    def sieve_load(self, name):
        (status, script) = self.sieve.getscript(name)
        if status[0] != "OK":
            self._sieve_errdlg(status)
            return None
        return script

    def sieve_delete(self, name):
        (status,) = self.sieve.deletescript(name)
        if status[0] != "OK":
            self._sieve_errdlg(status)
            return 1
        return 0

    def sieve_save(self, name, script):
        (status,) = self.sieve.putscript(name, script)
        if status[0] != "OK":
            self._sieve_errdlg(status)
            return 1
        return 0

    #########################################################################
    ### Main Logic

    def _init_logic(self):
        self.loggedon = 0

    def startup(self):
        v = LoginView.LoginView(self)
        self.set_master(v, _("Login"))
        self.set_status(_("Please login!"))

    def login(self, password):
        if not self.sieve_login(password):
            v = ServerView.ServerView(self)
            self.set_master(v, _("Script list"))
            self.loggedon = 1
            self.set_status(_("Login successfull."))

    def list(self):
        return self.sieve_list()

    def activate(self, name):
        if self.question(
            _(
                'Do you really want to activate script "%s"?\n\nThat would mean disabling the script currently activated!'
            )
            % name
        ):
            return
        if not self.sieve_activate(name):
            self.set_status(_('Script "%s" activated.') % name)

    def new(self):
        name = _("*Unnamed*")
        v = ScriptView.ScriptView(self, None, None)
        self.add_slave(v, name)
        self.set_status(u"")

    def edit(self, name):
        script = self.sieve_load(name)
        if script is not None:
            v = ScriptView.ScriptView(self, name, script)
            self.add_slave(v, name)
            self.set_status(u"")

    def delete(self, name):
        if self.question(_('Do you really want to delete script "%s"?') % name):
            return
        if not self.sieve_delete(name):
            self.set_status(_('Script "%s" deleted.') % name)

    def save(self, win, name, script):
        if not name:
            dlg = Dialogs.NameDialog(self.mainwin)
            r = dlg.run()
            dlg.destroy()
            if r != gtk.RESPONSE_OK:
                return
            name = dlg.name

        if not self.sieve_save(name, script):
            self.master_view.refresh()
            self.rename_slave(win, name)
            self.set_status(_('Script "%s" saved.') % name)

    def close(self, win):
        self.del_slave(win)

    def logout(self):
        for s in self.slave_view:
            s.on_close()
        self.loggedon = 0
        self.startup()

    def shutdown(self):
        if self.loggedon:
            self.logout()
        gtk.main_quit()
