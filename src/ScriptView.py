#
# This Python library is a part of
#   gsieve: a Sieve script editor
#
# Copyright 2001,2002 Karsten Petersen
#
# This library is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Author(s): Karsten Petersen <kapet@kapet.de>
#

import gtk, pango
import GladeWidget


class ScriptView(GladeWidget.GladeWidget):
    def __init__(self, model, name, script):
        self.initialize("ScriptView")
        self.model = model

        self.textbuffer = gtk.TextBuffer(None)
        self.textview = self.get_widget("textview")
        self.textview.set_buffer(self.textbuffer)

        font = pango.FontDescription()
        font.set_family("Courier")
        self.textview.modify_font(font)

        self.name = name
        if script:
            self.textbuffer.set_text(script)

    def on_close(self, *args):
        self.model.close(self)

    def on_save(self, *args):
        start, end = self.textbuffer.get_bounds()
        script = self.textbuffer.get_text(start, end, 0)
        self.model.save(self, self.name, script)
