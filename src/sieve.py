#
# This Python library is a part of
#   gsieve: a Sieve script editor
#
# Copyright 2001,2002 Karsten Petersen
#
# This library is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this library; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#
# Author(s): Karsten Petersen <kapet@kapet.de>
#

# Bugs in timsieved v1.0.0 from cyrus v2.0.16:
#   - no CRLF handling after CAPABILITY command
#   - HAVESPACE not accepting size as number

import sys, os
from socket import *
from string import *
import re
from base64 import encodestring

# -----------------------------------------------------------------------------


class Buffer:
    def __init__(self, reader, writer):
        self.r = reader
        self.w = writer
        self.rbuff = ""
        self.rlen = 0
        self.rpos = 0
        self.lastline = 0

    def write(self, data):
        #        print "<<<<<< >%s<" % repr(data)
        self.w(data)

    def writeline(self, data):
        self.write(data + "\r\n")

    def _add(self):
        data = self.r(1024)
        self.rbuff = self.rbuff + data
        self.rlen = self.rlen + len(data)

    #        print ">>>>>> >%s<" % repr(data)

    def read(self, n):
        while self.rlen < n:
            self._add()
        data = self.rbuff[self.rpos : self.rpos + n]
        self.rpos = self.rpos + n
        self.rlen = self.rlen - n
        self.last = n
        return data

    def unread(self):
        self.rpos = self.rpos - self.last
        self.rlen = self.rlen + self.last
        self.last = 0

    def readword(self):
        n = 1
        while 1:
            if self.rlen < n:
                self._add()
                continue
            if self.rbuff[self.rpos + (n - 1)] not in whitespace:
                n = n + 1
                continue
            n = n - 1
            break
        data = self.rbuff[self.rpos : self.rpos + n]
        self.rpos = self.rpos + n
        self.rlen = self.rlen - n
        self.last = n
        return data


# -----------------------------------------------------------------------------


class Sieve:
    def __init__(self, host="localhost", port=2000):
        self.server = (host, port)
        self.socket = None
        self.io = None

    def __del__(self):
        if self.io:
            self.logout()
        if self.socket:
            self.socket.close()

    # ----------------------

    def _read_sp(self):
        while 1:
            c = self.io.read(1)
            if c != " ":
                break
        self.io.unread()

    def _read_crlf(self):
        l = self.io.read(2)
        if l != "\r\n":
            self.io.unread()
            raise Exception

    def _read_string(self):
        c = self.io.read(1)
        if c == "{":
            l = ""
            while 1:
                c = self.io.read(1)
                if c == "}":
                    break
                l = l + c
            self._read_crlf()
            if l[-1] == "+":
                l = l[:-1]
            n = int(l)
            data = self.io.read(n)
            return data
        elif c == '"':
            data = ""
            while 1:
                c = self.io.read(1)
                if c == "\\":
                    c = self.io.read(1)
                elif c == '"':
                    break
                data = data + c
            return data
        else:
            raise Exception

    def _read_oknobye(self):
        oknobye = self.io.readword()
        resp_code = ""
        desc = ""

        c = self.io.read(1)
        self.io.unread()
        if c == " ":
            x = 0
            self._read_sp()
            c = self.io.read(1)
            if c == "(":
                while 1:
                    c = self.io.read(1)
                    if c == ")":
                        break
                    resp_code = resp_code + c
                c = self.io.read(1)
                if c == " ":
                    self._read_sp()
                    x = 1
            else:
                self.io.unread()
                x = 1

            if x:
                desc = self._read_string()

        self._read_crlf()

        return (oknobye, resp_code, desc)

    # ----------------------

    def _read_capability(self):
        caps = {}
        while 1:
            c = self.io.read(1)
            self.io.unread()
            if c not in '{"':
                break

            key = self._read_string()

            c = self.io.read(1)
            if c == " ":
                self._read_sp()
                val = self._read_string()
            else:
                self.io.unread()
                val = ""

            caps[key] = val

            self._read_crlf()

        return caps

    # ----------------------

    def login(self):
        if self.socket:
            self.logout()

        self.socket = socket(AF_INET, SOCK_STREAM)
        self.socket.connect(self.server)

        self.io = Buffer(self.socket.recv, self.socket.send)

        caps = self._read_capability()
        status = self._read_oknobye()

        return (status, caps)

    def logout(self):
        self.io.writeline("LOGOUT")
        status = self._read_oknobye()
        self.socket.close()
        self.socket = None
        self.io = None
        return (status,)

    def capability(self):
        self.io.writeline("CAPABILITY")

        caps = self._read_capability()
        status = self._read_oknobye()

        # this is just dumb - timesieved from cyrus 2.0.16 does not clear
        # the following CRLF, so it returns an additional oknobye line
        if caps["IMPLEMENTATION"] == "Cyrus timsieved v1.0.0":
            error = self._read_oknobye()

        return (status, caps)

    def authenticate(self, user, passwd, proxyuser=None):
        if not proxyuser:
            proxyuser = user
        userpass = "%s\0%s\0%s" % (proxyuser, user, passwd)
        euserpass = encodestring(userpass)
        self.io.writeline('AUTHENTICATE "PLAIN" {%u+}' % len(euserpass))
        self.io.writeline("%s" % euserpass)
        status = self._read_oknobye()
        return (status,)

    def listscripts(self):
        self.io.writeline("LISTSCRIPTS")
        scripts = []
        while 1:
            c = self.io.read(1)
            self.io.unread()
            if c not in '{"':
                break

            name = self._read_string()

            c = self.io.read(1)
            self.io.unread()
            active = 0
            if c == " ":
                self._read_sp()
                x = self.io.readword()
                if x == "ACTIVE":
                    active = 1

            scripts.append((name, active))

            self._read_crlf()

        status = self._read_oknobye()
        return (status, scripts)

    def havespace(self, scriptname, size):
        # buggy ?!   let putscript complain
        return (("OK", "", "havespace not working"),)

    #        self.io.writeline('HAVESPACE \"%s\" %u' % (scriptname, size))
    #        status = self._read_oknobye()
    #        return (status,)

    def getscript(self, scriptname):
        self.io.writeline('GETSCRIPT "%s"' % scriptname)
        c = self.io.read(1)
        self.io.unread()
        if c in '{"':
            data = self._read_string()
            self._read_crlf()
        else:
            data = None

        status = self._read_oknobye()
        return (status, data)

    def putscript(self, scriptname, scriptdata):
        self.io.writeline('PUTSCRIPT "%s" {%u+}' % (scriptname, len(scriptdata)))
        self.io.writeline(scriptdata)

        status = self._read_oknobye()
        return (status,)

    def setactive(self, scriptname):
        self.io.writeline('SETACTIVE "%s"' % scriptname)

        status = self._read_oknobye()
        return (status,)

    def deletescript(self, scriptname):
        self.io.writeline('DELETESCRIPT "%s"' % scriptname)

        status = self._read_oknobye()
        return (status,)


# -----------------------------------------------------------------------------

if __name__ == "__main__":
    import getpass

    if len(sys.argv) < 2:
        print("Usage: %s <host> [<port>]" % sys.argv[0])
        sys.exit(-1)
    host = sys.argv[1]
    port = 2000
    if len(sys.argv) > 2:
        port = int(sys.argv[1])
    s = Sieve(host, port)

    print("# login")
    print(s.login())

    print("\n# authenticate")
    print(s.authenticate(getpass.getuser(), getpass.getpass()))

    print("\n# capability")
    print(s.capability())

    print("\n# havespace")
    print(s.havespace("test", 256))

    print("\n# putscript")
    print(s.putscript("test", "# this is a test script\012keep;\012"))

    print("\n# listscripts")
    print(s.listscripts())

    #    print '\n# setactive'
    #    print s.setactive('test')

    #    print '\n# listscripts'
    #    print s.listscripts()

    print("\n# getscript")
    print(s.getscript("test"))

    print("\n# deletescript")
    print(s.deletescript("test"))

    print("\n# logout")
    print(s.logout())
