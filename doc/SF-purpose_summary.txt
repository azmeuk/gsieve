The Cyrus IMAP daemon features a serverside scripting language called "Sieve"
that already is is an official internet standard (RFC 3028).
Have a look at the Sieve website:  http://www.cyrusoft.com/sieve/

Sieve is able to analyze incoming mail, sort it into some mailbox, throw it
away, forward it or send an automatic response.
Many Cyrus sites probably have the Sieve subsystem running, but the number of
users is often rather low because it is not so easy to edit the scripts.

Until now, there exists not one simple and free GUI editor for Sieve scripts,
only commercial ones, WWW or Emacs based.

This editor will be realized in Python 2.2, using GTK2 and Glade2 for GUI
creation.  (Well, it is mostly working and already realized this way...)
My target platform is Linux, but it should run on every platform that features
the programs named above.

