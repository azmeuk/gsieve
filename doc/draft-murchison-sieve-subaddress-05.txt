






Network Working Group                                        K. Murchison
Document: draft-murchison-sieve-subaddress-05.txt      Oceana Matrix Ltd.
Expires January 5, 2003                                      30 June 2002


                        Sieve -- Subaddress Extension


Status of this Memo

    This document is an Internet-Draft and is in full conformance with
    all provisions of Section 10 of RFC2026.  Internet-Drafts are working
    documents of the Internet Engineering Task Force (IETF), its areas,
    and its working groups.  Note that other groups may also distribute
    working documents as Internet-Drafts.

    Internet-Drafts are draft documents valid for a maximum of six months
    and may be updated, replaced, or obsoleted by other documents at any
    time.  It is inappropriate to use Internet-Drafts as reference
    material or to cite them other than as "work in progress."

    The list of current Internet-Drafts can be accessed at
    http://www.ietf.org/ietf/1id-abstracts.txt

    To view the list Internet-Draft Shadow Directories, see
    http://www.ietf.org/shadow.html.

    Distribution of this memo is unlimited.

Copyright Notice

    Copyright (C) The Internet Society 2002. All Rights Reserved.


Abstract

    On email systems that allow for "subaddressing" or "detailed
    addressing" (eg, "ken+sieve@example.org"), it is sometimes desirable
    to make comparisons against these sub-parts of addresses.  This draft
    defines an extension to the Sieve mail filtering language that allows
    users to compare against the user and detail parts of an address.










Expires January 5, 2003       Murchison                         [Page 1]

Internet Draft        Sieve -- Subaddress Extension        June 30, 2003


                           Table of Contents



Status of this Memo  . . . . . . . . . . . . . . . . . . . . . . . .   1

Copyright Notice . . . . . . . . . . . . . . . . . . . . . . . . . .   1

Abstract . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .   1

0.     Meta-information on this draft  . . . . . . . . . . . . . . .   3

0.1.   Discussion  . . . . . . . . . . . . . . . . . . . . . . . . .   3

0.2.   Noted Changes . . . . . . . . . . . . . . . . . . . . . . . .   3

0.2.1  since -04 . . . . . . . . . . . . . . . . . . . . . . . . . .   3

0.2.2  since -03 . . . . . . . . . . . . . . . . . . . . . . . . . .   3

0.2.3  since -02 . . . . . . . . . . . . . . . . . . . . . . . . . .   3

0.2.4  since -01 . . . . . . . . . . . . . . . . . . . . . . . . . .   4

0.2.5  since -00 . . . . . . . . . . . . . . . . . . . . . . . . . .   4

1.     Introduction  . . . . . . . . . . . . . . . . . . . . . . . .   4

2.     Capability Identifier . . . . . . . . . . . . . . . . . . . .   4

3.     Subaddress Comparisons  . . . . . . . . . . . . . . . . . . .   5

4.     Security Considerations . . . . . . . . . . . . . . . . . . .   6

5.     Acknowledgments . . . . . . . . . . . . . . . . . . . . . . .   6

6.     Author's Address  . . . . . . . . . . . . . . . . . . . . . .   6

Appendix A.  References  . . . . . . . . . . . . . . . . . . . . . .   7

Appendix B.  Full Copyright Statement  . . . . . . . . . . . . . . .   7










Expires January 5, 2003       Murchison                         [Page 2]

Internet Draft        Sieve -- Subaddress Extension        June 30, 2003



0.     Meta-information on this draft

    This information is intended to facilitate discussion.  It will be
    removed when this document leaves the Internet-Draft stage.


0.1.   Discussion

    This draft is intended to be an extension to the Sieve mail filtering
    language, available from the RFC repository as
    <ftp://ftp.ietf.org/rfc/rfc3028.txt>.

    This draft and the Sieve language itself are being discussed on the
    MTA Filters mailing list at <ietf-mta-filters@imc.org>.  Subscription
    requests can be sent to <ietf-mta-filters-request@imc.org> (send an
    email message with the word "subscribe" in the body).  More
    information on the mailing list along with a WWW archive of back
    messages is available at <http://www.imc.org/ietf-mta-filters/>.


0.2.   Noted Changes


0.2.1  since -04

    Non-existent ":detail" sub-part causes test to fail.

    Editorial changes.


0.2.2  since -03

    Editorial changes.


0.2.3  since -02

    Decided on :user and :detail as sub-parts (removed Known Issues).

    Noted that separator character MUST match the one used by the mail
    system.

    Editorial changes.







Expires January 5, 2003       Murchison                         [Page 3]

Internet Draft        Sieve -- Subaddress Extension        June 30, 2003



0.2.4  since -01

    Added description of subaddressing inspired by Randy Gellens.

    Added discussion of handling different separator characters.

    Editorial changes.


0.2.5  since -00

    Added augmented ADDRESS-PART syntax element.

    Editorial changes.


1.  Introduction

    Subaddressing is the practice of appending some "detail" information
    to the local-part of an [IMAIL] address to indicate that the message
    should be delivered to the set of messages specified by the "detail"
    information.  The "detail" information is prefixed with a special
    "separator character" (typically "+") which forms the boundary
    between the "user" (original local-part) and the "detail" sub-parts
    of the address, much like the "@" character forms the boundary
    between the local-part and domain.

    Typical uses of subaddressing might be:

    - A message addressed to "ken+sieve@example.org" is delivered into a
    mailbox called "sieve" belonging to the user "ken".

    - A message addressed to "5551212#123@example.org" is delivered to
    the voice mailbox number "123" at phone number "5551212".

    This document describes an extension to the Sieve language defined by
    [SIEVE] for comparing against the "user" and "detail" sub-parts of an
    address.

    Conventions for notations are as in [SIEVE] section 1.1, including
    use of [KEYWORDS].


2.  Capability Identifier

    The capability string associated with the extension defined in this
    document is "subaddress".



Expires January 5, 2003       Murchison                         [Page 4]

Internet Draft        Sieve -- Subaddress Extension        June 30, 2003


3.  Subaddress Comparisons

    Commands that act exclusively on addresses may take the optional
    tagged arguments ":user"  and ":detail" to specify what sub-part of
    the local-part of the address will be acted upon.

    NOTE: In most cases, the envelope "to" address is the prefered
    address to examine for subaddress information when the desire is to
    sort messages based on how they were addressed so as to get to a
    specific recipient.  The envelope address is, after all, the reason a
    given message is being processed by a given sieve script for a given
    user.  This is particularly true when mailing lists, aliases, and
    "virtual domains" are involved since the envelope may be the only
    source of detail information for the specific recipient.

    The ":user" argument specifies that sub-part of the local-part which
    lies to the left of the separator character (eg, "ken" in
    "ken+sieve@example.org").  If no separator character exists, then
    ":user" specifies the entire left-side of the address (equivalent to
    ":localpart").

    The ":detail" argument specifies that sub-part of the local-part
    which lies to the right of the separator character (eg, "sieve" in
    "ken+sieve@example.org").  If no separator characater exists, the the
    test evaluates to false.  If nothing lies to the right of the
    separator character, then ":detail" ":is" the null key ("").
    Otherwise, the ":detail" sub-part contains the null key.

    Implementations MUST make sure that the separator character matches
    that which is used and/or allowed by the encompassing mail system,
    otherwise unexpected results might occur.  Implementations SHOULD
    allow the separator character to be configurable so that they may be
    used with a variety of mail systems.

    The ":user" and ":detail" address parts are subject to the same rules
    and restrictions as the standard address parts defined in [SIEVE].
    For convenience, the "ADDRESS-PART" syntax element defined in [SIEVE]
    is augmented here as follows:

         ADDRESS-PART  =/  ":user" / ":detail"


    A diagram showing the ADDRESS-PARTs of a email address utilizing a
    separator character of '+' is shown below:

           :user "+" :detail  "@" :domain
          `-----------------'
              :local-part



Expires January 5, 2003       Murchison                         [Page 5]

Internet Draft        Sieve -- Subaddress Extension        June 30, 2003


    Example:

          require "subaddress";

          # File mailing list messages (subscribed as "ken+mta-filters").
          if envelope :detail "to" "mta-filters" {
            fileinto "inbox.ietf-mta-filters";
          }

          # If a message is not to me (ignoring +detail), junk it.
          if not allof (address :user ["to", "cc", "bcc"] "ken",
               address :domain ["to", "cc", "bcc"] "example.org") {
            discard;
          }

          # Redirect all mail sent to +foo.
          if envelope :detail ["to", "cc", "bcc"] "foo" {
            redirect "ken@example.edu";
          }


4.  Security Considerations

    Security considerations are discussed in [SIEVE].  It is believed
    that this extension doesn't introduce any additional security
    concerns.


5.  Acknowledgments

    Thanks to Tim Showalter, Alexey Melnikov, Michael Salmon, Randall
    Gellens, Philip Guenther and Jutta Degener for their help with this
    document.


6.  Author's Address

    Kenneth Murchison
    Oceana Matrix Ltd.
    21 Princeton Place
    Orchard Park, NY 14127

    Phone: (716) 662-8973
    EMail: ken@oceana.com







Expires January 5, 2003       Murchison                         [Page 6]

Internet Draft        Sieve -- Subaddress Extension        June 30, 2003


Appendix A.  References

     [IMAIL] Resnick, P., Ed., "Internet Message Format", QUALCOMM
         Incorporated, RFC 2822, April 2001.


     [KEYWORDS] Bradner, S., "Key words for use in RFCs to Indicate
         Requirement Levels", Harvard University, RFC 2119, March 1997.


     [SIEVE] Showalter, T., "Sieve: A Mail Filtering Language", Mira�
         point, Inc., RFC 3028, January 2001.


Appendix B.  Full Copyright Statement

    Copyright (C) The Internet Society 2002. All Rights Reserved.

    This document and translations of it may be copied and furnished to
    others, and derivative works that comment on or otherwise explain it
    or assist in its implementation may be prepared, copied, published
    and distributed, in whole or in part, without restriction of any
    kind, provided that the above copyright notice and this paragraph
    are included on all such copies and derivative works.  However, this
    document itself may not be modified in any way, such as by removing
    the copyright notice or references to the Internet Society or other
    Internet organizations, except as needed for the purpose of develop-
    ing Internet standards in which case the procedures for copyrights
    defined in the Internet Standards process must be followed, or as
    required to translate it into languages other than English.

    The limited permissions granted above are perpetual and will not be
    revoked by the Internet Society or its successors or assigns.

    This document and the information contained herein is provided on an
    "AS IS" basis and THE INTERNET SOCIETY AND THE INTERNET ENGINEERING
    TASK FORCE DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, INCLUDING
    BUT NOT LIMITED TO ANY WARRANTY THAT THE USE OF THE INFORMATION
    HEREIN WILL NOT INFRINGE ANY RIGHTS OR ANY IMPLIED WARRANTIES OF
    MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.











Expires January 5, 2003       Murchison                         [Page 7]

