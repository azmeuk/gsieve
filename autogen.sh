#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

origdir=`pwd`
cd $srcdir

DIE=0

test -z "$AUTOMAKE" && AUTOMAKE=automake-1.7
test -z "$ACLOCAL" && ACLOCAL=aclocal-1.7
test -z "$AUTOCONF" && AUTOCONF=autoconf

(autoconf --version) < /dev/null > /dev/null 2>&1 || {
	echo
	echo "You must have autoconf installed to compile gsieve."
	echo "Download the appropriate package for your distribution,"
	echo "or get the source tarball at ftp://ftp.gnu.org/pub/gnu/"
	DIE=1
}

($AUTOMAKE --version) < /dev/null > /dev/null 2>&1 || {
	echo
	echo "You must have automake installed to compile gsieve."
	echo "Download the appropriate package for your distribution,"
	echo "or get the source tarball at ftp://ftp.gnu.org/pub/gnu/"
	DIE=1
}

if test "$DIE" -eq 1; then
	exit 1
fi

test -f src/gsieve.in || {
	echo "You must run this script in the top-level gsieve directory."
	exit 1
}

if test -z "$*"; then
	echo "I am going to run ./configure with no arguments - if you wish "
	echo "to pass any to it, please specify them on the $0 command line."
fi

$ACLOCAL -I m4
$AUTOMAKE -a
$AUTOCONF

cd $origdir
$srcdir/configure --enable-maintainer-mode "$@"

echo
echo "Now type 'make' to compile gsieve."

